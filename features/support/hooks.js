/**
 * Created by jsethi on 1/26/2016.
 */
var hooks = function () {

    this.Before(function (scenario, callback) {

        //Extract the tags supplied to the scenario
        var callee_arguments = arguments.callee.caller.arguments[2];
        var tags = callee_arguments.getTags();
        var scenarioTags = [];

        //If there are no tags then push phantom else check the tags for the browser instances requested. Else load
        //all the tags into the scenarioTags array
        if (tags.length === 0){
            scenarioTags.push("@phantom");
        }
        else {
            tags.forEach(function (element){
                scenarioTags.push(element.getName());
            });
        }

        //Next we shall go through each tag processed, and fetch the required configuration
        var cbi = this.BrowserInstances;
        var browserInstanceArray = this.browserInstanceArray;
        var wdioClient = this.wdioClient;
        var getClient = this.getClient;
        var q = require('q');

        //Create an array of promises
        var tagPromises = [];
        scenarioTags.forEach(function (element) {
            tagPromises.push(cbi.createBrowserInstance(element));
        });

        //Using Q.All to resolve all of them, capture the value, and finally use callback.
        q.all(tagPromises)
        .then(function (result) {
            result.forEach(function (element) {
                var objBrowserConfig = element.valueOf();
                if (objBrowserConfig) {
                    objBrowserConfig.scenario = callee_arguments.getName();
                    objBrowserConfig.feature = callee_arguments.getUri();
                    browserInstanceArray.push(objBrowserConfig);
                }
            })
            return browserInstanceArray;
        })
        .then (function (browserInstanceArray) {
            //It might be a possibility that the tags passed to the scenarios, or features have not been recognized
            //and in this case no browser instances are started by the project. Let's move with our headless testing.
            if (browserInstanceArray.length === 0) {
                var createPhantomInstance = cbi.createBrowserInstance('@phantom').then(function (response) {
                    var phantomConfig = response;
                    phantomConfig.scenario = callee_arguments.getName();
                    phantomConfig.feature = callee_arguments.getUri();
                    browserInstanceArray.push(phantomConfig);
                    return ('phantom');
                });
                return createPhantomInstance;
            }
        })
        .done(function (response){
            console.log("Browsers Instances Being Tested (" + browserInstanceArray.length + ")");
            setTimeout( function () {
                wdioClient.push(getClient(browserInstanceArray, callee_arguments.getName(), callee_arguments.getUri()));
                wdioClient[0].init(callback);
            }, (3500 * browserInstanceArray.length));
        });

    });

    this.After(function (scenario, callback) {

        //Now we shall kill the processes created for the scenarios, because we are done with them.
        var browserInstanceArray = this.browserInstanceArray;
        var callee_arguments = arguments.callee.caller.arguments[2];
        var wdioClient = this.wdioClient;

        function destroyInstances () {
            browserInstanceArray.forEach(function (element) {
                var objInstance = element.valueOf();
                if (objInstance.scenario == callee_arguments.getName() && objInstance.feature == callee_arguments.getUri()) {
                    if (objInstance.seleniumProcess.pid !== null) {
                        process.kill(objInstance.seleniumProcess.pid);
                    }
                    process.kill(objInstance.process.pid);
                }
            });
            callback();
        }

        wdioClient[0].end(destroyInstances);

    });
};

module.exports = hooks;