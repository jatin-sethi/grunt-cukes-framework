/**
 * Created by jsethi on 2/1/2016.
 */
var configure = function () {
    this.setDefaultTimeout(30 * 1000);
};

module.exports = configure;