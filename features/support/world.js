/**
 * Created by jsethi on 1/20/2016.
 */

var World = function (){

    var query = require('array-query');
    var wdio = require('webdriverio');

    this.getClient = function (configArray, scenario, feature) {

        var objArray = query('scenario').is(scenario).and('feature').is(feature).on(configArray);
        if (objArray.length == 1) {

            return wdio.remote(objArray[0].configuration);
        }
        else if (objArray.length > 1) {
            var multiremoteConfig = {};
            objArray.forEach(function(element, index) {
                var objSettings =  element.valueOf();
                multiremoteConfig[index + "-" + objSettings.process.name] = objSettings.configuration;
            });

            return wdio.multiremote(multiremoteConfig);
        }
    };

    this.BrowserInstances = require(process.cwd() + '/selenium-manager/driver-seleniumInstance-binder');
    this.browserInstanceArray = [];
    this.wdioClient = [];

};

module.exports = function () {this.World = World; };