@login
Feature: Login (Aditya)

  Scenario: Jatin tries to login with a valid username and invalid password.
    Given Jatin is already on the Outlook Login Page.
    When  Jatin enters "jatinsethi@outlook.com" in username, and "SomeWrongPassword" as password.
    Then  Jatin should be shown an error message.

  Scenario: Aditya tries to login with a valid username and valid password.
    Given Jatin is already on the Outlook Login Page.
    When  Jatin enters "jatinsethi@outlook.com" in username, and "SomeCorrectPassword" as password.
    Then  Aditya should should be able to login.
