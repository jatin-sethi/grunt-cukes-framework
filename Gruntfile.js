/*
 * grunt-cucumberjs
 * https://github.com/mavdi/cucumberjs
 *
 * Copyright (c) 2013 Mehdi Avdi
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);

    // Configuration to be run (and then tested).
    grunt.initConfig({

        cucumberjs: {
            "Aditya Logs In": ["features/Login-Aditya.feature"],
            "Jatin Logs In": ["features/Login-Jatin.feature"],
            "Login Complete": ["features/Login-Aditya.feature","features/Login-Jatin.feature"],
            options: {
                steps: "features",
                format: 'html',
				saveJson: true,
				theme: 'bootstrap'
            }
        },

        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            features: ['cucumberjs:Aditya Logs In','cucumberjs:Jatin Logs In'],
            featureSet: ['cucumberjs:Login Complete']
        }
    });

    grunt.registerTask('default', ['cucumberjs']);
    grunt.registerTask('parallel', ['concurrent:features']);
    grunt.registerTask('series', ['concurrent:featureSet']);
};
