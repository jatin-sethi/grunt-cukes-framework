var q = require('q');
var request = require('request');
var driverConfiguration = require('./driver-configuration').driverConfig;
var seleniumInstanceManager = require('./selenium-instance-manager').processManager;
var os = require('os');

/* Step 1: Fetch all the IPv4 external ifaces */

var interfaces = os.networkInterfaces();
var IPv4Array = [];
var IPv4Address = '';

Object.keys(interfaces).forEach(
	function (interfaceName) {
		//Loop through each Interface Object found in the Named IP Interfaces until the program finds first external IPv4 address.
		interfaces[interfaceName].forEach(
			function (ipInterface) {
				if (ipInterface.family === 'IPv4' && ipInterface.internal === false) {
					IPv4Array.push(ipInterface);
				}
			}
		);
	}
);

//In case there is just one external IPv4 address, immediately assign it to IPv4Address variable as selenium jar will create instances using this.
//Else pick up the last one because we will have two external IPv4 address only in case of VPN/Proxy where the actual IPv4 would be the last one.

if (IPv4Array.length === 1) {
	IPv4Address = IPv4Array[0].address;
} else {
	IPv4Address = IPv4Array[IPv4Array.length - 1].address;
}

/* Step 2: This will combine a couple of steps */

var seleniumActive = function() {

	var defer = q.defer();
	request('http://' + IPv4Address + ':4444/grid/console', function (error, response, body) {
		if (error || response.statusCode !== 200) {
			defer.resolve(false);
		} else {
			defer.resolve(true);
		}
	});

	return defer.promise;
};

var browserInstanceGenerator = function (instanceGenerator, driverConfig) {

	var browserInstance = {seleniumProcess : null, process : null, configuration : null};
	var defer = q.defer();

	if (instanceGenerator === null && driverConfig === null){
		defer.resolve(false);
		return defer.promise;
	}

	seleniumActive()
		.then( function(response) {

			if (!response) {
				browserInstance.seleniumProcess = seleniumInstanceManager.startSelenium();
			} else {
				browserInstance.seleniumProcess = null;
			}
			browserInstance.process = instanceGenerator();
			browserInstance.configuration = driverConfig;
			browserInstance.configuration.port = browserInstance.process.port;
			browserInstance.configuration.host = IPv4Address;
		})
		.done( function () {
			defer.resolve(browserInstance);
		});

	return defer.promise;
};

var createBrowserInstance = function (BrowserName) {

	switch (BrowserName) {

		case '@firefox':
			return browserInstanceGenerator(seleniumInstanceManager.createFirefoxInstance, driverConfiguration.Firefox);

		case '@chrome':
			return browserInstanceGenerator(seleniumInstanceManager.createChromeInstance, driverConfiguration.Chrome);

		case '@phantom':
			return browserInstanceGenerator(seleniumInstanceManager.createPhantomInstance, driverConfiguration.Phantom);

		case '@edge':
			return browserInstanceGenerator(seleniumInstanceManager.createEdgeInstance, driverConfiguration.Edge);

		default:
			return browserInstanceGenerator(null, null);
	}
};

//noinspection JSUnresolvedVariable
exports.createBrowserInstance = createBrowserInstance;