/*
	This wrapper will be used to create the selenium server instance, and will return new browser nodes generated on run time.
	The selenium server, and browser nodes will be created as 'Detached Processes', and could be created or killed anytime.
	The object processManager will contain all the functions require to deal with initialization of the termination of the nodes.
	The initial state of each process will be marked as active, and the user should mark it inactive once the execution has finished.
*/
var spawn = require('child_process').spawn;
var path = process.cwd();
var processManager = {};

//Function to generate unique port for each browser instance first initial port being 4999.
var port = function () {
	var portCounter = 4999;
	return function () { return portCounter += 1; };
}();

//Function to start the Selenium Grid on port 4444
processManager.startSelenium = function () {
	
	var args = [];
	args.push('-jar');
	args.push('./selenium-executables/standalone-servers/selenium.jar');
	args.push('-role');
	args.push('hub');
	args.push('-host');
	args.push('localhost');
	args.push('-port');
	args.push('4444');

	var selenium = spawn('java', args, {stdio: ['ignore', 'ignore', 'ignore'] , detached : true});
	selenium.unref();

	return ({
		name : 'selenium',
		port : 4444,
		pid : selenium.pid,
		status : 'Active'
	});
};

//Function to start the chrome browser instance, and connect it to the Selenium Grid
processManager.createChromeInstance = function () {

	var args = [];
	args.push('-jar');
	args.push('./selenium-executables/standalone-servers/selenium.jar');
	args.push('-role');
	args.push('node');
	args.push('-hub');
	args.push('http://localhost:4444/grid/register');
	args.push('-port');
	var initPort = port(); args.push(initPort);
	args.push('-browser');
	args.push('browserName=chrome');
	args.push('-Dwebdriver.chrome.driver='+ path +'\\selenium-executables\\drivers\\chromedriver.exe'); /* Update the driver path here*/

	var chrome = spawn('java', args, {stdio: ['ignore', 'ignore', 'ignore'] , detached : true});
	chrome.unref();

	return ({
		name : 'Chrome',
		port : initPort,
		pid : chrome.pid,
		status : 'Active'
	});
};

//Function to start the internet explorer instance, and connect it to the Selenium Grid
processManager.createEdgeInstance = function () {

	var args = [];
	args.push('-jar');
	args.push('./selenium-executables/standalone-servers/selenium.jar');
	args.push('-role');
	args.push('node');
	args.push('-hub');
	args.push('http://localhost:4444/grid/register');
	args.push('-port');
	var initPort = port(); args.push(initPort);
	args.push('-browser');
	args.push('browserName=MicrosoftEdge');
	args.push('-Dwebdriver.edge.driver="'+ path +'\\selenium-executables\\drivers\\IEDriverServer.exe'); /* Update the driver path here*/

	var edge = spawn('java', args, {stdio: ['ignore', 'ignore', 'ignore'] , detached : true});
	edge.unref();

	return ({
		name : 'Microsoft-Edge',
		port : initPort,
		pid : edge.pid,
		status : 'Active'
	});
};

//Function to start the Phantom JS instance, and connect it to the Selenium Grid
processManager.createPhantomInstance = function () {

	var args = [];
	args.push('-jar');
	args.push('./selenium-executables/standalone-servers/selenium.jar');
	args.push('-role');
	args.push('node');
	args.push('-hub');
	args.push('http://localhost:4444/grid/register');
	args.push('-port');
	var initPort = port(); args.push(initPort);
	args.push('-browser');
	args.push('browserName=phantomjs');

	var phantom = spawn('java', args, {stdio: ['ignore', 'ignore', 'ignore'] , detached : true});
	phantom.unref();

	return ({
		name : 'Phantom-JS',
		port : initPort,
		pid : phantom.pid,
		status : 'Active'
	});
};

processManager.createFirefoxInstance = function () {

	var args = [];
	args.push('-jar');
	args.push('./selenium-executables/standalone-servers/selenium.jar');
	args.push('-role');
	args.push('node');
	args.push('-hub');
	args.push('http://localhost:4444/grid/register');
	args.push('-port');
	var initPort = port(); args.push(initPort);
	args.push('-browser');
	args.push('browserName=firefox');
	
	var firefox = spawn('java', args, {stdio: ['ignore', 'ignore', 'ignore'] , detached : true});
	firefox.unref();
	
	return ({
		name : 'Firefox',
		port : initPort, 
		pid : firefox.pid,
		status : 'Active'
	});
};


exports.processManager = processManager;