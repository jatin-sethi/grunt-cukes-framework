var driverConfig = {};

//Defining the chrome driver
driverConfig.Chrome = {
    host: '',
    port: 0,
    desiredCapabilities : {
        browserName : 'chrome',
        takesScreenshot	: true,
        javascriptEnabled : true,
        acceptSslCerts : true
    }
};

//Defining the firefox driver
driverConfig.Firefox = {
    host: '',
    port: 0,
    desiredCapabilities : {
        browserName : 'firefox',
        takesScreenshot	: true,
        javascriptEnabled : true,
        acceptSslCerts : true
    }
};

//Defining the internet explorer driver
//Changes from the UTAS Script -> Browsername changed to MicrosoftEdge, -> DWebdriver.ie.driver changed to Dwebdriver.edge.driver
driverConfig.Edge = {
    host: '',
    port: 0,
    desiredCapabilities : {
        browserName : 'MicrosoftEdge',
		takesScreenshot	: true,
        javascriptEnabled : true,
        acceptSslCerts : true

    }
};

//Defining the phantomJS driver
//Setup for the PhantomJS. Not provided in the PhantomJS. Must contain the property 'phantom.binary.path' in the object created. 
//'phantom.binary.path' assumes the phantomjs driver in the same directory from which the phantom has been launched from the grid.
driverConfig.Phantom = {
    host: '',
    port: 0,
    desiredCapabilities : {
        browserName : 'phantomjs',
        'phantomjs.binary.path' : process.cwd() + '\\selenium-executables\\drivers\\phantomjs.exe',
		takesScreenshot	: true,
        javascriptEnabled : true,
        acceptSslCerts : true

    }
};

//exports.getDriverConfig = function () {return driverConfig};
exports.driverConfig = driverConfig;